import React from "react";

export default function APropos() {
  return (
    <>
      <h1 className="display-4">Les details de notre app...</h1>
      <p className="lead">Ici pour la partie Contacts j'utilise l'API de context et des routes imbriquées en utilisant Outlet de React Rooter</p>
    </>
  );
}
